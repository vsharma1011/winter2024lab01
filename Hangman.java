import java.util.Scanner;
public class Hangman{

	public static void main(String[] arg){
		//***for testing***
		//System.out.println("Letter is in index " + isLetterInWord("FOWL",'X'));
		//System.out.println("Letter: " + toUpperCase('a'));
		//printWork("LEPT", false, false, false, false);
		
		Scanner reader = new Scanner(System.in);
		System.out.println("Enter a 4 letter word(IN CAPS):");
		String userInput = reader.nextLine();
		
		//Converts word to uppercase
		userInput = userInput.toUpperCase();
		
		//Runs main game
		runGame(userInput);
		
	}
	//Checks if guessed letter is in word & checks position
	public static int isLetterInWord(String word, char correctLetter){
		for(int i = 0; i< word.length(); i++){
			if(word.charAt(i) == correctLetter){
				return i;
			}
		}
		return -1;
	}
	//Converts the correctly guessed chars into uppercase
	public static char toUpperCase(char letter){
		return Character.toUpperCase(letter);
	}
	//Displays what words have been guessed and what are empty
	public static void printWork(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3){
		//All letters start off as a blank
		char firstChar = '_'; 
		char secondChar = '_'; 
		char thirdChar = '_'; 
		char forthChar = '_';
		//Changes a letter if it is guessed correctly
		if(letter0){
			firstChar = word.charAt(0);
		}
		if(letter1){
			secondChar = word.charAt(1);
		}
		if(letter2){
			thirdChar = word.charAt(2);
		}
		if(letter3){
			forthChar = word.charAt(3);
		}
		//Prints the current words guessed
		System.out.println("Your result is: "+ (firstChar +" "+secondChar+" "+thirdChar+" "+forthChar));

	}
	//Combines all prev functions to run game
	public static void runGame(String word){
		boolean guessPos0 = false;
		boolean guessPos1 = false;
		boolean guessPos2 = false;
		boolean guessPos3 = false;
			
		int maxTries = 5;
		
		//Checks if input was correct or not & displays that info to user
		while(maxTries >= 0 && (guessPos0 == false || guessPos1 == false || guessPos2 == false || guessPos3 == false)){
	
			Scanner reader = new Scanner(System.in);
			System.out.println("Guess a letter:");
			char guess = reader.next().charAt(0);
			int letterPos = isLetterInWord(word, toUpperCase(guess));
			
			//If letter is not in word, you lose a try
			if(letterPos == -1){
				System.out.println("Wrong Choice, "+maxTries+" tries left");
				maxTries--;
			}
			else{
				System.out.println("Letter is in index " + letterPos);
				if(letterPos == 0){
					guessPos0 = true;
				}
				if(letterPos == 1){
					guessPos1 = true;
				}
				if(letterPos == 2){
					guessPos2 = true;
				}
				if(letterPos == 3){
					guessPos3 = true;
				}
				//Prints the result based on previous statement
				printWork(word, guessPos0,guessPos1,guessPos2,guessPos3);
			}		
		}
		//Checks maxTries to see if you have won or lost 
		if(maxTries < 0){
			System.out.println("You Lose, The word was " + word);
		}
		else{
			System.out.println("You Win!");
		}
	}
}